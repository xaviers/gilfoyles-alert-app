import { ErrorHandler, Injectable } from '@angular/core';
import { AuthStore } from '@core/store/auth-store';
import { LoadingStore } from '@core/store/loading-store';
import { RouterStore } from '@core/store/router.store';
import { AlertModuleStore } from '@modules/alert/store';
import { ViewModuleStore } from '@modules/view/store';
import { Actions as StoreActions, Effect } from '@ngrx/effects';
import { StoreFailureAction } from '@shared/models/error/store-failure-action.model';
import { filter, map } from 'rxjs/operators';

export namespace AppStore {

  export interface State {
    auth: AuthStore.State;
    alert: AlertModuleStore.State;
    loading: LoadingStore.State;
    view: ViewModuleStore.State;
    router: RouterStore.State;
  }

  export const reducers = {
    auth: AuthStore.reducer,
    router: RouterStore.reducer,
    loading: LoadingStore.reducer
  };
}

@Injectable()
export class AppStoreEffects {

  constructor(private actions$: StoreActions, private errorHandler: ErrorHandler) {}

  @Effect({dispatch: false})
  errorHandler$ = this.actions$.pipe(
    filter((action: any) => action instanceof StoreFailureAction),
    map((action: StoreFailureAction) => action.payload),
    map(payload => this.errorHandler.handleError(payload)));

}
