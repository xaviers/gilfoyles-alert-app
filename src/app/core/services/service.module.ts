import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { MatSnackBarModule } from '@angular/material';
import { AlertPriceWatchService } from '@core/services/alert/alert-price-watch.service';
import { AlertWatchService } from '@core/services/alert/alert-watch.service';
import { CryptoCompareInterceptorService } from '@core/services/crypto-compare/crypto-compare-interceptor.service';
import { CryptoCompareService } from '@core/services/crypto-compare/crypto-compare.service';
import { AppErrorHandler } from '@core/services/error/app-error-handler.service';
import { LoadingInterceptorService } from '@core/services/http/loading-interceptor.service';
import { SnackBarService } from '@core/services/snackbar/snackbar.service';
import { StorageService } from '@core/services/storage/storage.service';

@NgModule({
  imports: [
    MatSnackBarModule
  ],
  providers: [
    AlertPriceWatchService,
    AlertWatchService,
    CryptoCompareService,
    SnackBarService,
    StorageService,
    {provide: ErrorHandler, useClass: AppErrorHandler},
    {provide: HTTP_INTERCEPTORS, useClass: LoadingInterceptorService, multi: true},
    {provide: HTTP_INTERCEPTORS, useClass: CryptoCompareInterceptorService, multi: true}
  ]
})
export class ServiceModule {
}
