import { Injectable } from '@angular/core';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarRef, SimpleSnackBar } from '@angular/material';

@Injectable()
export class SnackBarService {

  constructor(private snackBar: MatSnackBar) {}

  public show(message: string, panelClass?: string): MatSnackBarRef<SimpleSnackBar> {
    const config: MatSnackBarConfig = {
      duration: 3000,
      horizontalPosition: 'right',
      verticalPosition: 'bottom',
      panelClass: panelClass || 'success-snackbar'
    };

    return this.snackBar.open(message, undefined, config);
  }

  public showError(error = new Error()) {
    this.show('Something bad happened.', 'error-snackbar');
  }

}
