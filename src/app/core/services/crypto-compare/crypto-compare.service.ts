import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { AppProperties } from '@app/app.properties';
import { CryptoCompareAllExchangesResponse, HistoricalDataResponse, Pair } from '@shared/models';
import { Map } from 'immutable';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class CryptoCompareService {

  constructor(private http: HttpClient) {}

  allExchanges(): Observable<CryptoCompareAllExchangesResponse> {
    const params = new HttpParams()
      .set('extraParams', AppProperties.APP_NAME);

    return this.http.get<CryptoCompareAllExchangesResponse>(`${AppProperties.URL.CRYPTOCOMPARE_API}/data/v2/all/exchanges`, {params: params}).pipe(
      map(CryptoCompareAllExchangesResponse.fromJson)
    );
  }

  singleSymbolPrice(pair: Pair, exchangeName: string): Observable<Map<string, number>> {
    const params = new HttpParams()
      .set('fsym', pair.baseCurrency)
      .set('tsyms', pair.quoteCurrency)
      .set('e', exchangeName)
      .set('extraParams', AppProperties.APP_NAME);

    return this.http.get<Map<string, number>>(`${AppProperties.URL.CRYPTOCOMPARE_API}/data/price`, {params: params}).pipe(
      map(response => Map(response))
    );
  }

  minuteHistoricalData(pair: Pair, exchangeName: string, limit: number): Observable<HistoricalDataResponse> {
    const params = new HttpParams()
      .set('fsym', pair.baseCurrency)
      .set('tsym', pair.quoteCurrency)
      .set('e', exchangeName)
      .set('limit', limit.toString());

    return this.http.get<HistoricalDataResponse>(`${AppProperties.URL.CRYPTOCOMPARE_API}/data/histominute`, {params: params}).pipe(
      map(HistoricalDataResponse.fromJson)
    );
  }

}
