import { NotificationStore } from '@modules/view/store/notification.store';
import { SideNavStore } from '@modules/view/store/sidenav.store';

export namespace ViewModuleStore {

  export interface State {
    sideNav: SideNavStore.State;
    notification: NotificationStore.State;
  }

  export const reducers = {
    sideNav: SideNavStore.reducer,
    notification: NotificationStore.reducer
  };
}
